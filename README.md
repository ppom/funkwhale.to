# funwhale.tld/to/

Inside a Funkwhale federation, we want to share songs by a clickable link.
This is the workaround I found.

I serve this directory like this with nginx:
```nginx
# [...]
server {
	server_name music.ppom.me;
	# [...]
	location /to {
		index index.html;
		root /path/to/this/repo;
	}
	# [...]
}
# [...]
```

It permits to provide a link to a track search on the receiver's configured Funkwhale instance (stored in a cookie for convenience).
