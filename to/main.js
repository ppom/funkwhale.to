// vim: sw=4

window.onload = function () {

// Si tu sets plusieurs cookies bet c'est raté
const cookieSplit = decodeURIComponent(document.cookie).split("; ").find(row => row.startsWith("instance="));
const cookie = cookieSplit ? cookieSplit.split("=")[1] : "";

console.log("cookies", document.cookie);
console.log("cookie", cookie);

// Voilà voilà, d'aucun·es diront que cette ligne ne sert à rien. C'est vrai.
const fragment = window.location.hash.slice(1);

const noinstance = document.getElementById("noinstance");
const instanceform = document.getElementById("instanceform");
const instance = document.getElementById("instance");
const sharelink = document.getElementById("sharelink");

const nofragment = document.getElementById("nofragment");
const term = document.getElementById("term");
const copy = document.getElementById("copy");

instanceform.addEventListener('submit', e => {
    e.preventDefault();
    document.cookie = `instance=${e.currentTarget.instance.value}`;
    window.location.reload(true);
});

term.addEventListener('keydown', e => {
    setTimeout(() => sharelink.value = `${window.location.href}#${encodeURIComponent(term.value)}`, 40);
});

copy.addEventListener('click', e => {
    navigator.clipboard.writeText(sharelink.value);
    copy.innerHTML = "Copié !";
    setTimeout(() => copy.innerHTML = "Copier", 1000);
});

if (!cookie.length) {
    // Ici on demande une instance
    // On cache la partie demande de terme
    nofragment.classList.add("disabled");
    term.disabled = true;
    copy.disabled = true;
} else if (fragment.length <= 1) {
    // Ici on demande un terme à chercher et on propose un lien
    // On cache la demande d'instance
    noinstance.classList.add("disabled");
    instance.disabled = true;
    instance.value = cookie;
} else {
    // Ici on redirige vers l'instance
    document.location.href = `https://${cookie}/search?q=${fragment}&type=tracks&page=1`
}

}
